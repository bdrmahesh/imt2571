<?php
include_once("IModel.php");
include_once("Book.php");

/** The Model is the class holding data about a collection of books. 
 * @author Rune Hjelsvold
 * @see http://php-html.net/tutorials/model-view-controller-in-php/ The tutorial code used as basis.
 */
class DBModel implements IModel
{        
    /**
      * The PDO object for interfacing the database
      *
      */
    protected $db = null;  
    
    /**
	 * @throws PDOException
     */
    public function __construct($db = null)  
    {  
	    if ($db) 
  		{
  			$this->db = $db;
  		}
  		else
  		{
        // Create PDO connection
        $this->db = new PDO('mysql:dbname=test;host=localhost', 'root', '', array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));
  		}
    }
    
    /** Function returning the complete list of books in the collection. Books are
     * returned in order of id.
     * @return Book[] An array of book objects indexed and ordered by their id.
	 * @throws PDOException
     */
    public function getBookList()
    {
        try{
      		$book = array();
          $booklist = $this->db->query('select * from book')->fetchAll();

          foreach ($booklist as $each) {
              $book[] = new Book($each['title'], $each['author'], $each['description'], $each['id']);
          }

          return $book;
        }catch(PDOException $e){
            return array();
            print_r($e->getMessage());exit();
        }
    }
    
    /** Function retrieving information about a given book in the collection.
     * @param integer $id the id of the book to be retrieved
     * @return Book|null The book matching the $id exists in the collection; null otherwise.
	 * @throws PDOException
     */
    public function getBookById($id)
    {
        $book = null;
        try{
            $get_book = $this->db->query('select * from book where id='.$id)->fetch();
            if($get_book)
              $book = new Book($get_book['title'], $get_book['author'], $get_book['description'], $get_book['id']);

            return $book;
        }catch(PDOException $e){
            return $book;
        }
    }
    
    /** Adds a new book to the collection.
     * @param $book Book The book to be added - the id of the book will be set after successful insertion.
	 * @throws PDOException
     */
    public function addBook($book)
    {
        try{
            if(!$this->validateInput($book)){
              return false;
            }
            $query = $this->db->prepare('insert into book (title, author, description) values (:title, :author, :description)');
            $query->execute(array(':title'=>$book->title, ':author'=>$book->author, ':description'=>$book->description));
            
            return true;
        }catch(PDOException $e){
            return false;
        }
    }

    /** Modifies data related to a book in the collection.
     * @param $book Book The book data to be kept.
     * @todo Implement function using PDO and a real database.
     */
    public function modifyBook($book)
    {
        try{
            if(!$this->validateInput($book)){
              return false;
            }else if(!$this->isIdInteger($book->id)){
              return false;
            }
            $query = $this->db->prepare('update book set title=:title, author=:author, description=:description where id=:id');
            $query->execute(array(':title'=>$book->title, ':author'=>$book->author, ':description'=>$book->description, ':id'=>$book->id));

            return true;
        }catch(PDOException $e){
            return false;
        }
    }

    /** Deletes data related to a book from the collection.
     * @param $id integer The id of the book that should be removed from the collection.
     */
    public function deleteBook($id)
    {
        try{
          if(!$this->isIdInteger($id)){
            return false;
          }
          $query = $this->db->prepare('delete from book where id=:id');
          $test = $query->execute(array(':id'=>$id));

          return true;
        }catch(PDOException $e){
            return false;
        }
    }

    public function validateInput($book){
      if(!isset($book->title) || $book->title == ""){
        return false;
      }else if(!isset($book->author) || $book->author == ""){
        return false;
      }
      return true;
    }

    public function isIdInteger($id){
      if (filter_var($id, FILTER_VALIDATE_INT) === false || $id < 1) {
        return false;
      }
      return true;
    }
	
}

?>